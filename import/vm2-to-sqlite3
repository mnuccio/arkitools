#!/bin/bash
set -euf -o pipefail

vm2dir="${1:?Cartella con i VM2?}"
[ -d "$vm2dir" ] || { echo "ERRORE: $vm2dir non è una cartella!" 1>&2 && exit 1; }
vm2table="$(basename "$vm2dir" | tr -s -- ' -' '_')"

find_vm2() {
  find "$vm2dir" -iname '*.vm2' "$@"
}

(
  cat <<-EOF
	create table "$vm2table" (
	  reftime timestamp not null,
	  id_stazione int not null,
	  id_variabile int not null,
	  val1 float,
	  val2 float,
	  val3 text,
	  flags varchar not null,
	  primary key (id_variabile, id_stazione, reftime),
	  constraint reftime_12cifre check (reftime glob '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	  constraint id_stazione_naturale check (id_stazione > 0 and id_stazione not glob '*[^0-9]*'),
	  constraint id_variabile_naturale check (id_variabile > 0 and id_variabile not glob '*[^0-9]*'),
	  constraint val1_reale check (val1 not glob '*[^-0-9.]*'),
	  constraint val2_reale check (val2 not glob '*[^-0-9.]*'),
	  constraint val3_alfanumerico check (val3 not glob '*[^-0-9.a-zA-Z]*'),
	  constraint flags_9cifre check (flags in ('000000000', '054000000', '200000000', '254000000', '100000000'))
	);
	EOF
  echo .mode csv
  find_vm2 -printf '.import "%p" "'"$vm2table"'"\n'
  echo .mode column
  echo "select 'Record caricati: ' || count(*) from \"""$vm2table""\";"
) | sqlite3 dati.sqlite3

echo 'Record in input:' "$(find_vm2 -exec cat '{}' \; | wc -l)"
