#!/usr/bin/env bash
set -eu -o pipefail
for inname in *.dvd
do
	[[ -e $inname ]] || { echo "Ignoro $inname"; continue; }
	indate=${inname%.dvd}
	mkdir -p "$indate"
	dvdcae2vm --timezone "GMT" --input-file "$inname" --multiple-files --output-file "$indate"/%05d
done
